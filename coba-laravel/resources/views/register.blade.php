@extends('layouts.main')

@section('judul')
Halaman Form
@endsection

@section('content')
<h1>Buat Account Baru</h1>
<h3>Sign Up Form</h3>
<form action="/welcome" method="get">
  @csrf
  <label for="fname">First name:</label><br><br>
  <input type="text" id="fname" name="fname"><br><br>
  <label for="lname">Last name:</label><br><br>
  <input type="text" id="lname" name="lname"><br><br>
  <label>Gender</label><br><br>
  <input type="radio" name="gender">Male <br>
  <input type="radio" name="gender">Female <br><br>
  <label>Nationality</label><br><br>
  <select>
    <option value="Indonesia">Indonesia</option>
    <option value="Palestine">Palestine</option>
    <option value="Syria">Syria</option>
  </select><br><br>
  <label>Language Spoken</label><br><br>
  <input type="checkbox" name="lang" value="Bahasa Indonesia">Bahasa Indonesia <br>
  <input type="checkbox" name="lang" value="english">English <br>
  <input type="checkbox" name="lang" value="other">Other <br><br>
  <label for="bio">Bio</label><br><br>
  <textarea name="bio" id="bio" cols="30" rows="7"></textarea><br>
  <input type="submit" value="Sign Up">
</form>
@endsection