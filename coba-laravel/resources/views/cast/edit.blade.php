@extends('layouts.main')

@section('judul')
Edit Cast {{$cast->id}}
@endsection

@section('breadcrumb')
Edit cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" name="nama" value="{{ old('nama', $cast->nama) }}" id="nama" placeholder="Masukkan Nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="text" class="form-control" name="umur" value="{{ old('umur', $cast->umur) }}" id="umur" placeholder="Masukkan Umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="bio">Bio</label>
        <input type="text" class="form-control" name="bio"  value="{{ old('bio', $cast->bio) }}"  id="bio" placeholder="Masukkan Bio">
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Edit</button>
</form>
@endsection