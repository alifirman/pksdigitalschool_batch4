@extends('layouts.main')

@section('judul')
Show Cast {{$cast->id}}
@endsection

@section('breadcrumb')
cast {{ $cast->id }}
@endsection

@section('content')
<h4>{{$cast->nama}}</h4>
<p>{{$cast->umur}} tahun</p>
<p>{{$cast->bio}}</p>
@endsection